import React from 'react';
import Dropzone from 'react-dropzone';
import ReactAvatarEditor from 'react-avatar-editor';

class ImageComposer extends React.Component {
  state = {
    image: 'avatar.jpg',
    scale: 1,
    preview: null,
    borderRadius: 0,
    rotationDegree: 0
  };

  handleNewImage = e => {
    this.setState({image: e.target.files[0]});
  };

  handleSave = data => {
    const img = this.editor.getImageScaledToCanvas().toDataURL();
    // const img = this.editor.getImage().toDataURL();

    this.setState({
      preview: {
        img
      }
    });
  };

  handleScale = e => {
    const scale = parseFloat(e.target.value);
    this.setState({scale});
  };

  handleBorderRadius = e => {
    const borderRadius = parseFloat(e.target.value);
    this.setState({borderRadius});
  };

  handleRotate = e => {
    const rotationDegree = parseFloat(e.target.value);
    this.setState({rotationDegree});
  };


  setEditorRef = editor => {
    if (editor) {
      this.editor = editor;
    }
  };

  handleDrop = acceptedFiles => {
    this.setState({image: acceptedFiles[0]});
  };

  render() {
    return (
      <div>
        <Dropzone
          onDrop={this.handleDrop}
          disableClick={true}
          multiple={false}
          style={{
            width: this.state.width,
            height: this.state.height,
            marginBottom: '35px'
          }}
        >
          {({getRootProps, getInputProps}) => (
            <div {...getRootProps()}>

              <ReactAvatarEditor
                ref={this.setEditorRef}
                scale={parseFloat(this.state.scale)}
                borderRadius={parseFloat(this.state.borderRadius)}
                rotate={parseFloat(this.state.rotationDegree)}
                width={250}
                height={250}
                image={this.state.image}
                className="editor-canvas"
              />
              {/* <input {...getInputProps()} /> */}
            </div>
          )}
        </Dropzone>
        <br />
        New File:
        <input name="newImage" type="file" onChange={this.handleNewImage} />
        <br />
        Zoom:
        <input
          name="scale"
          type="range"
          onChange={this.handleScale}
          min={this.state.allowZoomOut ? '0.1' : '1'}
          max="2"
          step="0.01"
          defaultValue="1"

        />

border:
        <input
          name="borderRadius"
          type="range"
          onChange={this.handleBorderRadius}
          min="0"
          max="200"
          step="5"
          defaultValue="1"
        />
        <br />

        Rotate:
        <input
          name="rotate"
          type="range"
          onChange={this.handleRotate}
          min="0"
          max="360"
          step="5"
          defaultValue="1"
        />
        <br />
        <br />
        <input type="button" onClick={this.handleSave} value="Preview" />
        <br />
        {!!this.state.preview && <img src={this.state.preview.img} />}
      </div>
    );
  }
}

export default ImageComposer;
