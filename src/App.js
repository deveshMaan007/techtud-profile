import React from 'react';
// import ImageComposer from './ImageComposer';
import './styles.css';
import ImageEditor from './imageEditor';

const App = () => (
  // <ImageComposer />
  <div className="container">
    <ImageEditor />
  </div>
);

export default App;

