const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackStrip = require('strip-loader');
const StatsPlugin = require('stats-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: ['babel-polyfill', path.resolve(__dirname, 'src')],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.min.js',
    publicPath: '/'
  },
  devServer: {
    contentBase: 'build',
    inline: true,
    host: process.env.IP,
    historyApiFallback: true,
    port: 8080
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      inject: 'body',
      hash: true,
      filename: 'index.html'
    }),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),

    new StatsPlugin('webpack.stats.json', {
      source: false,
      modules: false
    }),
    new Dotenv({path: path.resolve(__dirname, './.env')})
  ],
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          warnings: false,
          compress: {
            drop_console: true,
            warnings: false
          },
          output: {
            comments: false,
            beautify: false
          }
        }
      })
    ]
  },

  module: {
    rules: [
      // {
      //   test: /\.js$/,
      //   loader: WebpackStrip.loader('debug', 'console.log')
      // },
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          emitWarning: true,
          fix: true,
          emitError: true,
          configFile: './.eslintrc'
        }
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          plugins: ['transform-class-properties']
        }
      },
      {
        test: /\.json?$/,
        loader: 'json'
      },
      {
        test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.eot$|\.wav$|\.mp3$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'assets/',
          publicPath: 'assets/'
        }
      },
      {
        // For all .css files except from node_modules
        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader', {loader: 'css-loader'}]
      },
      {
        // For all .css files in node_modules
        test: /\.css$/,
        include: /node_modules/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.less$/,
        loader: 'style-loader!css-loader!less'
      },

      // SASS
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!sass-loader'
      }
    ]
  }
};
